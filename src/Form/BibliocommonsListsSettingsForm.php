<?php

namespace Drupal\bibliocommons_lists\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;

/**
 * Build settings form.
 */
class BibliocommonsListsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bibliocommons_lists_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bibliocommons_lists.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $types_options = [];
    $field_options = [];
    $config = $this->config('bibliocommons_lists.settings');

    // Details for grouping general settings fields.
    $details_general = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#open' => TRUE,
    ];

    $details_general['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => $this->t('BiblioCommons API key.'),
      '#default_value' => $config->get('api_key'),
      '#description' => t("The reading list content type."),
      '#required' => TRUE,
    ];

    $details_general['library'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Library Name'),
      '#description' => $this->t('Library name used in the URL for books when Bibliocommons has it wrong.'),
      '#default_value' => $config->get('library'),
      '#required' => TRUE,
    ];

    // Get all Node and fields
    $content_types = NodeType::loadMultiple();
    foreach ($content_types as $content_type) {
      $types_options[$content_type->id()] = $content_type->label();


      foreach (\Drupal::service('entity_field.manager')->getFieldDefinitions('node', $content_type->id()) as $field_name => $field_definition) {
        if (!empty($field_definition->getTargetBundle())) {
          $field_options[$field_name] = $field_definition->getLabel();
        }
      }
    }

    asort($types_options);

    $details_general['content_type'] = [
      '#type' => 'select',
      '#title' => t('Select the Reading List Content Type.'),
      '#options' =>  $types_options,
      '#description' => t("The reading list content type."),
      '#default_value' =>  $config->get('content_type'),
      '#required' => TRUE,
    ];

    asort($field_options);

    $details_general['field'] = [
      '#type' => 'select',
      '#title' => t('Select the Reading List Content Type.'),
      '#options' =>  $field_options,
      '#description' => t("The reading list content type."),
      '#default_value' =>  $config->get('field'),
      '#required' => TRUE,
    ];

    // Inserts the details for grouping general settings fields.
    $form['bibliocommons_lists'][] = $details_general;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->configFactory->getEditable('bibliocommons_lists.settings');
    $values = $form_state->cleanValues()->getValues();

    foreach ($values as $field_key => $field_value) {
      $settings->set($field_key, $field_value);
    }
    $settings->save();

    parent::submitForm($form, $form_state);
  }

}
