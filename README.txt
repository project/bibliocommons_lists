To use the module, create a content type and add a link field to it. Then go to the configuration for the module 
and select the content type you want to use for reading lists.

The BiblioCommons API does not support listing all of your reading lists. So, to add a reading list to your site, find 
the URL of a reading list and place the full URL into the configured link field. Once you save the reading list, 
the module will import the list into the body field of the node.
