<?php

/**
 * @file
 * Contains BiblioCommons Lists module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use \Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \NYPL\Bibliophpile\Client;

/**
 * Implements hook_help().
 */
function bibliocommons_lists_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {

    // Main module help for the BiblioCommons Lists module.
    case 'help.page.bibliocommons_lists':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('BiblioCommons Lists') . '</p>';

      return $output;

    default:
  }
}

/**
 * Implements hook_entity_view().
 *
 * Redirect the user to bibliocommons if they cannot edit this reading list.
 */
function bibliocommons_lists_entity_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  $config = \Drupal::config('bibliocommons_lists.settings');
  $list_type = $config->get('content_type');
  $library = $config->get('library');
  $list_field = $config->get('field');

  if ($entity->bundle() == $list_type && $view_mode == 'full') {
    $user = User::load(\Drupal::currentUser()->id());
    if ($entity->access('update', $user)) {
      return;
    };

    if (isset($build["field_bibliocommons_url"][0]["#url"])) {
    $url = $build["field_bibliocommons_url"][0]["#url"];
      $url = $url->setAbsolute()->toString();
      if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
        (new RedirectResponse($url))->send();
        exit();
      }
    }
  }
}

/**
 * Implements hook_entity_presave().
 *
 * Import the list from bibliocommons
 */
function bibliocommons_lists_entity_presave(EntityInterface $entity) {
  $body = '';
  $summary = '';

  $config = \Drupal::config('bibliocommons_lists.settings');

  $list_type = $config->get('content_type');
  if ($entity->bundle() == $list_type) {
    $api_key = $config->get('api_key');
    $list_field = $config->get('field');
    $library = $config->get('library');


    $list_url = $entity->{$list_field}->first()->getUrl()->toString();
    $base_name = basename($list_url);
    if (str_contains($base_name, '_')) {
      $base_name = substr($base_name, 0, strpos($base_name, '_'));

    }
    $client = new Client($api_key);
    $bc_list = $client->itemList($base_name);

    // Our editors wanted to set our own titles.
    
    //$entity->setTitle($bc_list->name());
    $list_items = $bc_list->items();
    $i = 0;
    foreach ($list_items as $item) {
      if (get_class($item) == 'NYPL\\Bibliophpile\\ListItemTitle') {

        // Swap the test URL with the real URL because BiblioCommons prints the test website in the api!
        $item_url = $item->item()->details();
        $item_url = str_replace("any.", $library . '.' , $item_url);
        if ($i < 3) {
          $summary .= '<div class="biblio-item">' . "\n"
            . '  <div class="bibliocommons-image">' . "\n"
            . '    <a href="' . $item_url .'"><img src="' . $item->item()->jacket_cover()->url . '" alt="'. $item->item()->name() . ' book cover"></a>'
            . '    </div>' . "\n"
            . '    <div class="biblio-item-details">' . "\n"
            . '      <div><a href="' . $item_url . '">' . $item->item()->name() . '</a></div>' . "\n"
            . '      <div>' . $item->item()->format()->name() . '</div>' . "\n"
            . '    </div>' . "\n"
            . '</div>' . "\n";
        }
        $body .= '<div class="biblio-item">' . "\n"
          . '  <div class="bibliocommons-image">' . "\n"
          . '    <a href="' . $item_url .'"><img src="' . $item->item()->jacket_cover()->url . '" alt="'. $item->item()->name() . ' book cover"></a>'
          . '    </div>' . "\n"
          . '    <div class="biblio-item-details">' . "\n"
          . '      <div><a href="' . $item_url . '">' . $item->item()->name() . '</a></div>' . "\n"
          . '      <div>' . $item->item()->format()->name() . '</div>' . "\n"
          . '    </div>' . "\n"
          . '</div>' . "\n";

        $i++;
      }
    }
    $entity->body->summary = $summary;
    $entity->body->value = $body;
    $entity->body->format = 'full_html';
  }
}

